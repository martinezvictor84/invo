<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Numeric;
use Phalcon\Forms\Element\check;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Numericality;

class PersonsForm extends Form
{
    /**
     * Initialize the persons form
     */
    public function initialize($entity = null, $options = array())
    {
        if (!isset($options['edit'])) {
            $element = new Text("id");
            $this->add($element->setLabel("Id"));
        } else {
            $this->add(new Hidden("id"));
        }

        $name = new Text("name");
        $name->setLabel("Name");
        $name->setFilters(['striptags', 'string']);
        $name->addValidators([
            new PresenceOf([
                'message' => 'Name is required'
            ])
        ]);
        $this->add($name);



        $age = new Numeric("age");
        $age->setLabel("Age");
        $age->setFilters(['int']);
        $age->addValidators([
            new Numericality([
                'message' => 'Age is required'
            ])
        ]);
        $this->add($age);

        $director = new Check("director");
        $director->setLabel("Film Director");
        $director->setAttributes(['value' => '1']);
        $this->add($director);

        $actor = new Check("actor");
        $actor->setLabel("Actor");
        $actor->setAttributes(['value' => '1']);
        $this->add($actor);
    }
}
