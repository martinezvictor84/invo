<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Date;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Numericality;
use Phalcon\Validation\Validator\StringLength;

class FilmsForm extends Form
{
    /**
     * Initialize the film form
     */
    public function initialize($entity = null, $options = array())
    {
        if (!isset($options['edit'])) {
            $element = new Text("id");
            $this->add($element->setLabel("Id"));
        } else {
            $this->add(new Hidden("id"));
        }

        $title = new Text("title");
        $title->setLabel("Title");
        $title->setFilters(['striptags', 'string']);
        $title->addValidators([
            new PresenceOf([
                'message' => 'Title is required'
            ])
        ]);
        $this->add($title);



        $description = new TextArea("description");
        $description->setLabel("Descripcion");
        $description->setFilters(['striptags', 'string']);
        $description->addValidators([
          new StringLength([
              'max'            => 200,
              'messageMaximum' => 'The Description is too short',
          ])
        ]);

        $this->add($description);
        $releaseDate = new Date("release_date");
        $releaseDate->setLabel("Release Date");
        $this->add($releaseDate);

        $director =  new Select('director', Persons::find(['director'=>'1']), [
            'using'      => ['id', 'name'],
            'useEmpty'   => true,
            'emptyText'  => '...',
            'emptyValue' => ''
        ]);
        $director->setLabel("Film Director");
        $this->add($director);

        $actor =  new Select('cast[]', Persons::find(['actor'=>'1']), [
            'using'      => ['id', 'name'],
            'useEmpty'   => true,
            'emptyText'  => '...',
            'emptyValue' => '',
            'multiple' =>'true'
        ]);
        $actor->setLabel("Cast");
        $this->add($actor);


    }
}
