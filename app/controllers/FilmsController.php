<?php
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

/**
 * FilmsController
 *
 * Manage operations for films
 */
class FilmsController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Films');
        parent::initialize();
    }

    public function indexAction()
    {
      $numberPage = $this->request->getQuery("page", "int");
      $numberPage = $numberPage ==null?1:$numberPage;

      $films = Films::find();

      if (count($films) == 0) {
          $this->flash->notice("The search did not find any products");
      }


      $paginator = new Paginator(array(
          "data"  => $films,
          "limit" => 10,
          "page"  => $numberPage
      ));

      $this->view->page = $paginator->getPaginate();
    }

    public function newAction()
    {
        $this->view->form = new FilmsForm(null, array('edit' => true));
    }

    public function createAction()
    {
      if (!$this->request->isPost()) {
        return $this->dispatcher->forward(
          [
            "controller" => "films",
            "action"     => "index",
          ]
        );
      }

      $form = new FilmsForm;
      $film = new Films();

      $data = $this->request->getPost();

      /*save cast*/

      $cast = array();
      foreach ($data['cast'] as $idPerson) {
        $c = new Cast();
        $c->person = $idPerson;
        $cast[] = $c;
      }

      $film->cast = $cast;

      /*validate form */
      if (!$form->isValid($data, $film)) {
        foreach ($form->getMessages() as $message) {
          $this->flash->error($message);
        }
        return $this->dispatcher->forward(
          [
            "controller" => "films",
            "action"     => "new",
          ]
        );
      }
      /*save data film*/
      $firebase = new Firebase();
      $keyFi = $firebase->createFilm($film);
      $film ->key_firebase=$keyFi;
      if ($film->save() == false) {
        $keyFi = $firebase->deleteFilm($film);
        foreach ($film->getMessages() as $message) {
          $this->flash->error($message);
        }

        return $this->dispatcher->forward(
          [
            "controller" => "films",
            "action"     => "new",
          ]
        );
      }
      $form->clear();

      $this->flash->success("Person was created successfully");

      return $this->dispatcher->forward([
          "controller" => "films",
          "action"     => "index",
          ]);
    }

    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $film = Films::findFirstById($id);
            if (!$film) {
                $this->flash->error("Films was not found");

                return $this->dispatcher->forward(
                    [
                        "controller" => "films",
                        "action"     => "index",
                    ]
                );
            }

            $this->view->form = new FilmsForm($film, array('edit' => true));
        }
    }

    public function saveAction()
    {
        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(
                [
                    "controller" => "films",
                    "action"     => "index",
                ]
            );
        }

        $id = $this->request->getPost("id", "int");

        $film = Films::findFirstById($id);
        if (!$film) {
            $this->flash->error("Film does not exist");

            return $this->dispatcher->forward(
                [
                    "controller" => "persons",
                    "action"     => "index",
                ]
            );
        }

        $form = new FilmsForm;
        $this->view->form = $form;

        $data = $this->request->getPost();

        if (!$form->isValid($data, $film)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(
                [
                    "controller" => "films",
                    "action"     => "edit",
                    "params"     => [$id]
                ]
            );
        }
        /*delete the cast*/
        $cast = $film->getCast();
        foreach ($cast as $c) {
          if ($c->delete() === false) {
            $messages = $c->getMessages();
            foreach ($messages as $message) {
                echo $message;
            }
            break;
          }
        }
        /*save the new cast*/
        $cast = array();
        foreach ($data['cast'] as $idPerson) {
          $c = new Cast();
          $c->person = $idPerson;
          $cast[] = $c;
        }
        $film->cast = $cast;

        if ($film->save() == false) {
            foreach ($person->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(
                [
                    "controller" => "products",
                    "action"     => "edit",
                    "params"     => [$id]
                ]
            );
        }

        $firebase = new Firebase();
        $keyFi = $firebase->updateFilm($film);

        $form->clear();

        $this->flash->success("Film was updated successfully");

        return $this->dispatcher->forward(
            [
                "controller" => "films",
                "action"     => "index",
            ]
        );

    }

    public function deleteAction($id)
    {
        $film = Films::findFirstById($id);
        if (!$film) {
            $this->flash->error("Film was not found");

            return $this->dispatcher->forward(
                [
                    "controller" => "films",
                    "action"     => "index",
                ]
            );
        }
        /*delete the cast*/
        $cast = $film->getCast();
        foreach ($cast as $c) {
          if ($c->delete() === false) {
            $messages = $c->getMessages();
            foreach ($messages as $message) {
                echo $message;
            }
            break;
          }
        }
        try {
          $firebase = new Firebase();

          if (!$film->delete()) {
            foreach ($film->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward([
                    "controller" => "films",
                    "action"     => "index",
                ]
            );
          }
          $keyFi = $firebase->deleteFilm($film);
          $this->flash->success("Film was deleted");
        }
        catch (\Exception $e) {
          $this->flash->success("Film can't delete");
        }

        return $this->dispatcher->forward(
            [
                "controller" => "films",
                "action"     => "index",
            ]
        );

    }

    public function pruebaAction(){
      try {
        $firebase = new Firebase();
        $firebase->createFilm(null);
      } catch (\Exception $e) {
        var_dump($e);die;
      }

    }
}
