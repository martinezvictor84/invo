<?php
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;


/**
 * PersonsController
 *
 * Manage operations for Persons
 */
class PersonsController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Persons');
        parent::initialize();
    }

    public function indexAction()
    {
      $numberPage = $this->request->getQuery("page", "int");
      $numberPage = $numberPage ==null?1:$numberPage;

      $persons = Persons::find();
      if (count($persons) == 0) {
          $this->flash->notice("The search did not find any products");
      }


      $paginator = new Paginator(array(
          "data"  => $persons,
          "limit" => 10,
          "page"  => $numberPage
      ));

      $this->view->page = $paginator->getPaginate();
    }

    public function newAction()
    {
        $this->view->form = new PersonsForm(null, array('edit' => true));
    }

    public function createAction()
    {
      if (!$this->request->isPost()) {
        return $this->dispatcher->forward(
          [
            "controller" => "persons",
            "action"     => "index",
          ]
        );
      }

      $form = new PersonsForm;
      $persons = new Persons();

      $data = $this->request->getPost();

      if (!$form->isValid($data, $persons)) {
        foreach ($form->getMessages() as $message) {
          $this->flash->error($message);
        }

        return $this->dispatcher->forward(
          [
            "controller" => "persons",
            "action"     => "new",
          ]
        );
      }
      $firebase = new Firebase();
      $keyFi = $firebase->createPerson($persons);
      $persons->key_firebase = $keyFi;
      if ($persons->save() == false) {
        $keyFi = $firebase->deletePerson($persons);
        foreach ($persons->getMessages() as $message) {
          $this->flash->error($message);
        }

        return $this->dispatcher->forward(
          [
            "controller" => "persons",
            "action"     => "new",
          ]
        );
      }

      $form->clear();

      $this->flash->success("Person was created successfully");

      return $this->dispatcher->forward(
        [
          "controller" => "persons",
          "action"     => "index",
          ]
          );
        }

    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $person = Persons::findFirstById($id);
            if (!$person) {
                $this->flash->error("Product was not found");

                return $this->dispatcher->forward(
                    [
                        "controller" => "products",
                        "action"     => "index",
                    ]
                );
            }

            $this->view->form = new PersonsForm($person, array('edit' => true));
        }
    }

    public function saveAction()
    {
        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(
                [
                    "controller" => "persons",
                    "action"     => "index",
                ]
            );
        }

        $id = $this->request->getPost("id", "int");

        $person = Persons::findFirstById($id);
        if (!$person) {
            $this->flash->error("Product does not exist");

            return $this->dispatcher->forward(
                [
                    "controller" => "persons",
                    "action"     => "index",
                ]
            );
        }

        $form = new PersonsForm;
        $this->view->form = $form;

        $data = $this->request->getPost();


        if (!$form->isValid($data, $person)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(
                [
                    "controller" => "persons",
                    "action"     => "edit",
                    "params"     => [$id]
                ]
            );
        }

        if ($person->save() == false) {
            foreach ($person->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(
                [
                    "controller" => "products",
                    "action"     => "edit",
                    "params"     => [$id]
                ]
            );
        }

        $firebase = new Firebase();
        $keyFi = $firebase->updatePerson($person);

        $form->clear();

        $this->flash->success("Product was updated successfully");

        return $this->dispatcher->forward(
            [
                "controller" => "persons",
                "action"     => "index",
            ]
        );
    }

    public function deleteAction($id)
    {
        $person = Persons::findFirstById($id);
        if (!$person) {
            $this->flash->error("Persons was not found");

            return $this->dispatcher->forward(
                [
                    "controller" => "persons",
                    "action"     => "index",
                ]
            );
        }
        try {
          $firebase = new Firebase();
          $personC = $person;
          if (!$person->delete()) {
            foreach ($person->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward([
                    "controller" => "persons",
                    "action"     => "index",
                ]
            );
          }
          $firebase->deletePerson($person);
          $this->flash->success("Person was deleted");
        }
        catch (\Exception $e) {
          $this->flash->success("Person can't delete");
        }

        return $this->dispatcher->forward(
            [
                "controller" => "persons",
                "action"     => "index",
            ]
        );

    }
}
