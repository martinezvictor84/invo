<?php

use Phalcon\Mvc\Model;

/**
 * Persons
 */
class Persons extends Model
{
	/**
	 * @var integer
	 */
	public $id;

	/**
	 * @var string
	 */
	public $name;

	/**
	 * @var integer
	 */
	public $age;

	/**
	 * @var string
	 */
	public $enaled;

	/**
	 * @var boolean
	 */
	public $director;
	/**
	 * @var boolean
	 */
	public $actor;

	/**
	 * @var string
	 */
	public $key_firebase;

	/**
	 * Persons initializer
	 */
	public function initialize()
	{
		$this->hasMany('id','Cast','person');
	}

}
