<?php

use Phalcon\Mvc\Model;

/**
 * Cast
 */
class Cast extends Model
{
	/**
	 * @var integer
	 */
	public $film;

	/**
	 * @var integer
	 */
	public $person;

	public function initialize()
	{
		$this->belongsTo('film', 'Films', 'id');

		$this->belongsTo('person', 'Persons', 'id');
	}

}
