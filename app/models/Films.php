<?php

use Phalcon\Mvc\Model;

/**
 * Persons
 */
class Films extends Model
{
	/**
	 * @var integer
	 */
	public $id;

	/**
	 * @var string
	 */
	public $title;

	/**
	 * @var string
	 */
	public $description;

	/**
	 * @var string
	 */
	public $key_firebase;

	/**
	 * @var int
	 */
	public $director;
	/**
	 * @var Datetime
	 */
	public $release_date;

	/**
	 * Films initializer
	 */
	public function initialize()
	{
		$this->belongsTo('director', 'Persons', 'id');
		$this->hasMany('id','Cast','film');
	}

}
