<div class="row">
  <div class="col-md-12">
    <h3>FILMS</h3>
    {#{dump(page.items)}#}
  </div>
</div>
<div class="row">
  <div class="col-md-2 col-md-offset-10">
    <a href="/films/new">
      <button type="button" name="button" class="btn btn-success">New Film</button>
    </a>
  </div>
</div>
<div class="row" style="margin-top:5px;">
  <table id="tFilms" class="table table-bordered table-striped">
    <thead>
      <tr>
        <td>Title</td>
        <td>Description</td>
        <td>Cast</td>
        <td>Directed By</td>
        <td>Release date</td>
      </tr>
    </thead>
    <tbody>
      {% for item in page.items %}
      <tr>
        <th>{{item.title}}</th>
        <th>{{item.description}}</th>
        <th>
          {% for cast in item.getCast() %}
            {{cast.getPersons().name }}{% if not loop.last %},{% endif %}
          {% endfor %}
        </th>
        <th>{{item.getPersons().name}}</th>
        <th>{{item.release_date}}</th>
        <th width="7%">{{ link_to("films/edit/" ~ item.id, '<i class="glyphicon glyphicon-edit"></i> Edit', "class": "btn btn-default") }}</th>
        <th width="7%">{{ link_to("films/delete/" ~ item.id, '<i class="glyphicon glyphicon-remove"></i> Delete', "class": "btn btn-default") }}</th>
      </tr>
      {% endfor %}
      <tbody>
          <tr>
              <td colspan="7" align="right">
                  <div class="btn-group">
                      {{ link_to("films/index", '<i class="icon-fast-backward"></i> First', "class": "btn") }}
                      {{ link_to("films/index?page=" ~ page.before, '<i class="icon-step-backward"></i> Previous', "class": "btn") }}
                      {{ link_to("films/index?page=" ~ page.next, '<i class="icon-step-forward"></i> Next', "class": "btn") }}
                      {{ link_to("films/index?page=" ~ page.last, '<i class="icon-fast-forward"></i> Last', "class": "btn") }}
                      <span class="help-inline">{{ page.current }} of {{ page.total_pages }}</span>
                  </div>
              </td>
          </tr>
      </tbody>
    </tbody>
  </table>
</div>
