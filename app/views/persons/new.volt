
{{ content() }}

{{ form("persons/create") }}

    <ul class="pager">
        <li class="previous pull-left">
            {{ link_to("persons/index", "&larr; Go Back") }}
        </li>
        <li class="pull-right">
            {{ submit_button("Save", "class": "btn btn-success") }}
        </li>
    </ul>

    <fieldset>

    {% for element in form %}
        {% if is_a(element, 'Phalcon\Forms\Element\Hidden') %}
            {{ element }}
        {% else %}
            <div class="form-group">
              <div class="row">
                <div class="col-md-3" style="text-align:right;">
                  {{ element.label() }}:
                </div>
                <div class="col-md-7" >
                  {{ element.render(['class': '','style':'width:100%']) }}
                </div>
              </div>
            </div>
        {% endif %}
    {% endfor %}

    </fieldset>

</form>
