<div class="row">
  <div class="col-md-12">
    <h3>PERSONS</h3>
  </div>
</div>
<div class="row">
  <div class="col-md-2 col-md-offset-10">
    <a href="/persons/new">
      <button type="button" name="button" class="btn btn-success">New Person</button>
    </a>
  </div>
</div>
<div class="row" style="margin-top:5px;">
  <table id="tFilms" class="table table-bordered table-striped">
    <thead>
      <tr>
        <td>Name</td>
        <td>Age</td>
        <td>Profession</td>
      </tr>
    </thead>
    <tbody>
      {% for item in page.items %}
      <tr>
        <th>{{ item.name }}</th>
        <th>{{ item.age }}</th>
        <th>{% if item.director == 1 %}Film Director{% endif %}{% if item.actor == 1 %}{% if item.director == 1 %}/{% endif %}Actor{% endif %}</th>
        <th width="7%">{{ link_to("persons/edit/" ~ item.id, '<i class="glyphicon glyphicon-edit"></i> Edit', "class": "btn btn-default") }}</th>
        <th width="7%">{{ link_to("persons/delete/" ~ item.id, '<i class="glyphicon glyphicon-remove"></i> Delete', "class": "btn btn-default") }}</th>
      </tr>
      {% endfor %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="7" align="right">
                <div class="btn-group">
                    {{ link_to("persons/index", '<i class="icon-fast-backward"></i> First', "class": "btn") }}
                    {{ link_to("persons/index?page=" ~ page.before, '<i class="icon-step-backward"></i> Previous', "class": "btn") }}
                    {{ link_to("persons/index?page=" ~ page.next, '<i class="icon-step-forward"></i> Next', "class": "btn") }}
                    {{ link_to("persons/index?page=" ~ page.last, '<i class="icon-fast-forward"></i> Last', "class": "btn") }}
                    <span class="help-inline">{{ page.current }} of {{ page.total_pages }}</span>
                </div>
            </td>
        </tr>
    </tbody>
  </table>
</div>
