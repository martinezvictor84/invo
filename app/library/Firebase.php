<?php
/**
 * connectin firebase
 */
 use Kreait\Firebase\Factory;
 use Kreait\Firebase\ServiceAccount;

class Firebase
{

  private $serviceAccount =null;
  private $firebase = null;
  private $database = null;

  function __construct(){
    $this->serviceAccount =  ServiceAccount::fromJsonFile(__DIR__.'/servicesAccount.json');
    $this->firebase = (new Factory)
        ->withServiceAccount($this->serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://filmsprueba.firebaseio.com')
        ->create();

    $this->database = $this->firebase->getDatabase();
  }

  public function createFilm($film){

    $cast = array();
    foreach ($film->getCast() as $c) {
      $cast[] = $c->person;
    }

    $newFilm = $this->database
        ->getReference('films')
        ->push([
            'id' => $film->id,
            'title' => $film->title,
            'description' => $film->description,
            'cast' =>  $cast,
            'generes' => 'none',
            'director' => $film->director,
            'release_date' =>$film->release_date
        ]);

    return $newFilm->getKey();
  }

  public function updateFilm($film){
    $cast = array();
    foreach ($film->getCast() as $c) {
      $cast[] = $c->person;
    }

    $newFilm = $this->database
        ->getReference("films/{$film->key_firebase}")
        ->update([
            'id' => $film->id,
            'title' => $film->title,
            'description' => $film->description,
            'cast' =>  $cast,
            'generes' => 'none',
            'director' => $film->director,
            'release_date' =>$film->release_date
        ]);

    return $newFilm->getKey();
  }

  public function deleteFilm($film){

    return $this->database->getReference("films/{$film->key_firebase}")->remove();
  }

  public function createPerson($person){
    $newPerson = $this->database
        ->getReference('persons')
        ->push([
            'id' => $person->id,
            'name' => $person->name,
            'age' => $person->age,
            'enabled' =>  $person->enaled,
            'director' => $person->director,
            'actor' => $person->actor
        ]);

    return $newPerson->getKey();
  }

  public function updatePerson($person){
    $newPerson = $this->database
        ->getReference("persons/{$person->key_firebase}")
        ->update([
            'id' => $person->id,
            'name' => $person->name,
            'age' => $person->age,
            'enabled' =>  $person->enaled,
            'director' => $person->director,
            'actor' => $person->actor
        ]);

    return $newPerson->getKey();
  }

  public function deletePerson($person){
    return $this->database->getReference("persons/{$person->key_firebase}")->remove();
  }

}


?>
